# 👻

Can also find me on [Github](https://github.com/alt4) (though I seldom use it, I don't like Actions) and run my own [SCM](https://git.northamp.fr/alt) for bigger/solo projects.
